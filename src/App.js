import React, {useState, useEffect} from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import { makeStyles, ThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { green, red } from '@material-ui/core/colors'
import './App.css';
import Welcome from './screens/Welcome'
import Info from './screens/Info'


const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#de4256'
    },
    secondary: {
      main: "#b78e51" 
    },
    background: {
      paper: '#008f47',
      default: '#e3c299'
    }
  }
})



function App() {
  const [isUserRegistered, setIsUserRegistered] = useState(false)
  useEffect(() => {
    const user = JSON.parse(localStorage.getItem('user'))
    console.log('user', user)
    console.log('new Date()', new Date())
    console.log('user.expiredAt > new Date()', user.expiredAt > new Date())
    
    if (user && new Date(user.expiredAt) > new Date()) setIsUserRegistered(true)
  }, [])

  console.log('isUserRegistered', isUserRegistered)
  return (
    <Router>
      <ThemeProvider theme={theme}>
      <CssBaseline />
        <div className="App">
          <Switch>
            <Route exact path="/">
              {isUserRegistered ? <Info /> : <Welcome />}
            </Route>
            <Route path="/info">
            {isUserRegistered ? <Info /> : <Welcome />}
            </Route>
            {/* <Route path="/company/:name">
            {isUserRegistered ? <CompanyProfile /> : <Welcome />}
          </Route> */}
          </Switch>

        </div>
      </ThemeProvider>
    </Router>


  );
}

export default App;
